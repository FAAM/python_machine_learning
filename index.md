# Home

Introducción básica al machine learning con python

## Material

El material está disponible en el siguiente [repositorio](https://gitlab.com/FAAM/python_machine_learning), para obtener el código de fuente basta con que ejecutes el siguiente comando:

```
https://gitlab.com/FAAM/python_machine_learning
```

## Contenidos

```{tableofcontents}
```