# Introduccion al Machine Learning
<a href="https://fralfaro.gitlab.io/python_machine_learning/"><img alt="Link a la Documentación" src="https://img.shields.io/badge/jupyter--book-link-brightgreen"></a>
[![pipeline status](https://gitlab.com/FAAM/python_machine_learning/badges/master/pipeline.svg)](https://gitlab.com/FAAM/python_machine_learning/-/commits/master)
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/FAAM%2Fpython_machine_learning/HEAD)


## Contenidos temáticos

- Análisis de regresión
- Análisis de clasificación
- Análisis no supervisados
- Feature engineer
- Overfitting


